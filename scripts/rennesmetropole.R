# <!-- coding: utf-8 -->
#
# quelques fonctions pour Rennes Métropole
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
mga  <- function() {
  source("geo/scripts/rennesmetropole.R");
}
#
# https://gis.stackexchange.com/questions/63577/joining-polygons-in-r
# http://www.nickeubank.com/wp-content/uploads/2015/10/RGIS2_MergingSpatialData_part2_GeometricManipulations.html
#
# les commandes permettant le lancement
Drive <- substr( getwd(),1,2)
baseDir <- sprintf("%s/web", Drive)
cfgDir <<- sprintf("%s/web/geo/RENNESMETROPOLE", Drive)
ignDir <<- sprintf("%s/bvi35/CouchesIGN", Drive)
texDir <<- sprintf("%s/web/geo/RENNESMETROPOLE", Drive)
varDir <- sprintf("%s/bvi35/CouchesRennesMetropole", Drive);
leafletDir <- sprintf("%s/leaflet/exemples", baseDir);
setwd(baseDir)
source("geo/scripts/mga.R");
source("geo/scripts/misc.R");
source("geo/scripts/misc_biolo.R");
source("geo/scripts/misc_couches.R");
source("geo/scripts/misc_faune.R");
source("geo/scripts/misc_fonds.R");
source("geo/scripts/misc_gdal.R");
source("geo/scripts/misc_osm.R");
source("geo/scripts/misc_tex.R");
source("geo/scripts/couches_opendatarennes.R");
#source("geo/scripts/rennesmetropole_cartes.R");
source("geo/scripts/rennesmetropole_couches.R");
#source("geo/scripts/rennesmetropole_faune.R");
if ( interactive() ) {
  print(sprintf("rennesmetropole.R interactif"))
# un peu de nettoyage
  graphics.off()
} else {
  print(sprintf("rennesmetropole.R console"))
}
